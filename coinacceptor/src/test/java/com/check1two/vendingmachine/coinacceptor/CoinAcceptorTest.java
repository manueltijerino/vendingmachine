/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author manueltijerino
 */
public class CoinAcceptorTest {
    CoinAcceptor coinAcceptor = new CoinAcceptor();
    Coin pennyMintedBefore1982 = Coin.getPennyMintedBefore1982();
    Coin pennyMintedAfter1982 = Coin.getPennyMintedAfter1982();
    Coin washer = Coin.getWasher();
    Coin nickle = Coin.getNickle();
    Coin dime = Coin.getDime();
    Coin quarter = Coin.getQuarter();
    
    @Test
    public void whenACoinAcceptorIsCreatedForTestingItIsNotNull() {
        assertNotNull(coinAcceptor);
    }
    
    @Test
    public void whenACoinAcceptorIsCreatedTheInitialAmountIsZeroAndDisplaySaysInsertCoin(){
        BigDecimal currentAmount = coinAcceptor.getCurrentAmount();
        assertEquals("0.00", String.valueOf(currentAmount));
        assertEquals("INSERT COIN", coinAcceptor.getDisplay());
    }
    
    @Test
    public void whenInvalidCoinsInsertedTheAmountIsZeroAndDisplaySaysInsertCoin(){
        coinAcceptor.insert(pennyMintedBefore1982);
        coinAcceptor.insert(pennyMintedAfter1982);
        coinAcceptor.insert(washer);
        BigDecimal currentAmount = coinAcceptor.getCurrentAmount();
        assertEquals("0.00", String.valueOf(currentAmount));
        assertEquals("INSERT COIN", coinAcceptor.getDisplay());
    }
    
    @Test
    public void whenValidCoinsInsertedTheAmountIsNotZeroAndDisplayIsUpdated(){
        coinAcceptor.insert(nickle);
        coinAcceptor.insert(dime);
        coinAcceptor.insert(quarter);
        BigDecimal currentAmount = coinAcceptor.getCurrentAmount();
        assertEquals("0.40", String.valueOf(currentAmount));
        assertEquals("0.40", coinAcceptor.getDisplay());
    }
    
    @Test
    public void whenResetIsCalledTheAmountResetsToZeroDisplaySaysInsertCoin(){
        coinAcceptor.insert(nickle);
        coinAcceptor.insert(dime);
        coinAcceptor.insert(quarter);
        coinAcceptor.reset();
        BigDecimal currentAmountAfterReset = coinAcceptor.getCurrentAmount();
        assertEquals("0.00", String.valueOf(currentAmountAfterReset));
        assertEquals("INSERT COIN", coinAcceptor.getDisplay());
    }
}
