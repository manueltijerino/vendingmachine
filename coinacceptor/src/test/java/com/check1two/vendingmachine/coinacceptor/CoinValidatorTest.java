/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author manueltijerino
 */
public class CoinValidatorTest {

    CoinValidator coinValidator = new CoinValidator();
    Coin pennyMintedBefore1982 = Coin.getPennyMintedBefore1982();
    Coin pennyMintedAfter1982 = Coin.getPennyMintedAfter1982();
    Coin nickle = Coin.getNickle();
    Coin dime = Coin.getDime();
    Coin quarter = Coin.getQuarter();
    Coin washer = Coin.getWasher();
    
    @Test
    public void whenACoinValidatorIsCreatedForTestingItIsNotNull() {
        assertNotNull(coinValidator);
    }
    
    @Test
    public void whenCoinValidatorValidatesAPennyThenResultIsPenny(){
        boolean isPennyMintedBefore1982 = coinValidator.isPenny(pennyMintedBefore1982);
        assertTrue(isPennyMintedBefore1982);
        boolean isPennyMintedAfter1982 = coinValidator.isPenny(pennyMintedAfter1982);
        assertTrue(isPennyMintedAfter1982);
    }
    
    @Test
    public void whenCoinValidatorValidatesANickleThenResultIsNickle(){
        boolean isNickle = coinValidator.isNickle(nickle);
        assertTrue(isNickle);
    }
    
    @Test
    public void whenCoinValidatorValidatesADimeThenResultIsDime(){
        boolean isDime = coinValidator.isDime(dime);
        assertTrue(isDime);
    }
    
    @Test
    public void whenCoinValidatorValidatesAQuarterThenResultIsQuarter(){
        boolean isQuarter = coinValidator.isQuarter(quarter);
        assertTrue(isQuarter);
    }
    
    @Test
    public void whenCoinValidatorValidatesAFakeCoinThenResultIsFalse(){
        boolean isValid = coinValidator.isValid(washer);
        assertFalse(isValid);
    }
    
    @Test
    public void whenCoinValidatorValidatesAPennyThenResultIsFalse(){
        boolean isPennyMintedBefore1982Valid = coinValidator.isValid(pennyMintedBefore1982);
        assertFalse(isPennyMintedBefore1982Valid);
        boolean isPennyMintedAfter1982Valid = coinValidator.isValid(pennyMintedAfter1982);
        assertFalse(isPennyMintedAfter1982Valid);
    }
    
    @Test
    public void whenCoinValidatorValidatesANickleThenResultIsTrue(){
        boolean isValid = coinValidator.isValid(nickle);
        assertTrue(isValid);
    }
    
    @Test
    public void whenCoinValidatorValidatesADimeThenResultIsTrue(){
        boolean isValid = coinValidator.isValid(dime);
        assertTrue(isValid);
    }
    
    @Test
    public void whenCoinValidatorValidatesAQuarterThenResultIsTrue(){
        boolean isValid = coinValidator.isValid(quarter);
        assertTrue(isValid);
    }
    
    @Test
    public void whenCoinValidatorValidatesAFakeCoinThenValueIsZeroCents(){
        double value = coinValidator.getValue(washer);
        assertEquals(0.00, value, 0);
    }
    
    @Test
    public void whenCoinValidatorValidatesAPennyThenValueIsZeroCents(){
        double valueOfPennyMintedBefore1982 = coinValidator.getValue(pennyMintedBefore1982);
        assertEquals(0.00, valueOfPennyMintedBefore1982, 0);
        double valueOfPennyMintedAfter1982 = coinValidator.getValue(pennyMintedAfter1982);
        assertEquals(0.00, valueOfPennyMintedAfter1982, 0);
    }
    
    @Test
    public void whenCoinValidatorValidatesANickleThenValueIsFiveCents(){
        double value = coinValidator.getValue(nickle);
        assertEquals(0.05, value, 0);
    }
    
    @Test
    public void whenCoinValidatorValidatesADimeThenValueIsTenCents(){
        double value = coinValidator.getValue(dime);
        assertEquals(0.10, value, 0);
    }
    
    @Test
    public void whenCoinValidatorValidatesAQuarterThenValueIsTwentyFiveCents(){
        double value = coinValidator.getValue(quarter);
        assertEquals(0.25, value, 0);
    }
    
    @Test
    public void whenCoinFactoryCreatesAPennyMintedBefore1982ThenPennyMintedBefore1982UsCoinPropertiesAreUsed(){
        assertEquals(USCoinProperties.PENNY_DIAMETER_MILLIMETER, pennyMintedBefore1982.getDiameter(), 0);
        assertEquals(USCoinProperties.PENNY_THICKNESS_MILLIMETER, pennyMintedBefore1982.getThickness(), 0);
        assertEquals(USCoinProperties.PENNY_WEIGHT_GRAMS_MINTED_BEFORE_1982, pennyMintedBefore1982.getWeight(), 0);
        assertEquals(0.01, USCoinProperties.PENNY_MONITARY_VALUE, 0);
    }
    
    @Test
    public void whenCoinFactoryCreatesAPennyMintedAfter1982ThenPennyMintedAfter1982UsCoinPropertiesAreUsed(){
        assertEquals(USCoinProperties.PENNY_DIAMETER_MILLIMETER, pennyMintedAfter1982.getDiameter(), 0);
        assertEquals(USCoinProperties.PENNY_THICKNESS_MILLIMETER, pennyMintedAfter1982.getThickness(), 0);
        assertEquals(USCoinProperties.PENNY_WEIGHT_GRAMS_MINTED_AFTER_1982, pennyMintedAfter1982.getWeight(), 0);
        assertEquals(0.01, USCoinProperties.PENNY_MONITARY_VALUE, 0);
    }
    
    @Test
    public void whenCoinFactoryCreatesANickleThenNickleUsCoinPropertiesAreUsed(){
        assertEquals(USCoinProperties.NICKLE_DIAMETER_MILLIMETER, nickle.getDiameter(), 0);
        assertEquals(USCoinProperties.NICKLE_THICKNESS_MILLIMETER, nickle.getThickness(), 0);
        assertEquals(USCoinProperties.NICKLE_WEIGHT_GRAMS, nickle.getWeight(), 0);
        assertEquals(0.05, USCoinProperties.NICKLE_MONITARY_VALUE, 0);
    }
    
    @Test
    public void whenCoinFactoryCreatesADimeThenDimeUsCoinPropertiesAreUsed(){
        assertEquals(USCoinProperties.DIME_DIAMETER_MILLIMETER, dime.getDiameter(), 0);
        assertEquals(USCoinProperties.DIME_THICKNESS_MILLIMETER, dime.getThickness(), 0);
        assertEquals(USCoinProperties.DIME_WEIGHT_GRAMS, dime.getWeight(), 0);
        assertEquals(0.10, USCoinProperties.DIME_MONITARY_VALUE, 0);
    }
    
    @Test
    public void whenCoinFactoryCreatesAQuarterThenQuarterUsCoinPropertiesAreUsed(){
        assertEquals(USCoinProperties.QUARTER_DIAMETER_MILLIMETER, quarter.getDiameter(), 0);
        assertEquals(USCoinProperties.QUARTER_THICKNESS_MILLIMETER, quarter.getThickness(), 0);
        assertEquals(USCoinProperties.QUARTER_WEIGHT_GRAMS, quarter.getWeight(), 0);
        assertEquals(0.25, USCoinProperties.QUARTER_MONITARY_VALUE, 0);
    }
    
}
