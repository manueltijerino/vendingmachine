/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

/**
 *
 * @author manueltijerino
 */
public class USCoinProperties {
    public static final double PENNY_MONITARY_VALUE = .01;
    public static final double NICKLE_MONITARY_VALUE = .05;
    public static final double DIME_MONITARY_VALUE = .10;
    public static final double QUARTER_MONITARY_VALUE = .25;
    
    public static final double PENNY_DIAMETER_MILLIMETER = 19.05;
    public static final double NICKLE_DIAMETER_MILLIMETER = 21.21;
    public static final double DIME_DIAMETER_MILLIMETER = 17.91;
    public static final double QUARTER_DIAMETER_MILLIMETER = 24.26;
    
    public static final double PENNY_THICKNESS_MILLIMETER = 1.52;
    public static final double NICKLE_THICKNESS_MILLIMETER = 1.95;
    public static final double DIME_THICKNESS_MILLIMETER = 1.35;
    public static final double QUARTER_THICKNESS_MILLIMETER = 1.75;
    
    public static final double PENNY_WEIGHT_GRAMS_MINTED_AFTER_1982 = 2.5;
    public static final double PENNY_WEIGHT_GRAMS_MINTED_BEFORE_1982 = 3.11;
    public static final double NICKLE_WEIGHT_GRAMS = 5.00;
    public static final double DIME_WEIGHT_GRAMS = 2.268;
    public static final double QUARTER_WEIGHT_GRAMS = 5.670;
}
