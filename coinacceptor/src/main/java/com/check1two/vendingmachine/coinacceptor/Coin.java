/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

/**
 *
 * @author manueltijerino
 */
public class Coin {
    private double diameter, thickness, weight;
    
    private Coin(double diameter, double thickness, double weight){
        this.diameter = diameter;
        this.thickness = thickness;
        this.weight = weight;
    }
    
    public static Coin getPennyMintedBefore1982(){
        return new Coin(USCoinProperties.PENNY_DIAMETER_MILLIMETER, USCoinProperties.PENNY_THICKNESS_MILLIMETER, USCoinProperties.PENNY_WEIGHT_GRAMS_MINTED_BEFORE_1982);
    }
    
    public static Coin getPennyMintedAfter1982(){
        return new Coin(USCoinProperties.PENNY_DIAMETER_MILLIMETER, USCoinProperties.PENNY_THICKNESS_MILLIMETER, USCoinProperties.PENNY_WEIGHT_GRAMS_MINTED_AFTER_1982);
    }
    
    public static Coin getNickle(){
        return new Coin(USCoinProperties.NICKLE_DIAMETER_MILLIMETER, USCoinProperties.NICKLE_THICKNESS_MILLIMETER, USCoinProperties.NICKLE_WEIGHT_GRAMS);
    }
    
    public static Coin getDime(){
        return new Coin(USCoinProperties.DIME_DIAMETER_MILLIMETER, USCoinProperties.DIME_THICKNESS_MILLIMETER, USCoinProperties.DIME_WEIGHT_GRAMS);
    }
    
    public static Coin getQuarter(){
        return new Coin(USCoinProperties.QUARTER_DIAMETER_MILLIMETER, USCoinProperties.QUARTER_THICKNESS_MILLIMETER, USCoinProperties.QUARTER_WEIGHT_GRAMS);
    }
    
    public static Coin getWasher(){
        return new Coin(7, 7, 7);
    }
    
    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
