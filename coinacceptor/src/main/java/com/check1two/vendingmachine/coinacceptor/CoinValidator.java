/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

import static com.check1two.vendingmachine.coinacceptor.USCoinProperties.*;

/**
 *
 * @author manueltijerino
 */
public class CoinValidator {

    public boolean isValid(Coin coin) {
        boolean isPenny = isPenny(coin);
        boolean isNickle = isNickle(coin);
        boolean isDime = isDime(coin);
        boolean isQuarter = isQuarter(coin);
        boolean isValid = isNickle || isDime || isQuarter && !isPenny;
        return isValid;
    }
    
    //note: pennies have value 0 in this coin validator since they are invalid
    public double getValue(Coin coin){
        double value = 0.00;
        if(isValid(coin)){
            if(isNickle(coin)){
                value = NICKLE_MONITARY_VALUE;
            }else if(isDime(coin)){
                value = DIME_MONITARY_VALUE;
            }else if(isQuarter(coin)){
                value = QUARTER_MONITARY_VALUE;
            }
        }
        return value;
    }
    
    public boolean isPenny(Coin coin){
        boolean isPenny = coin.getDiameter() == PENNY_DIAMETER_MILLIMETER && 
                coin.getThickness() == PENNY_THICKNESS_MILLIMETER && 
                (coin.getWeight() == PENNY_WEIGHT_GRAMS_MINTED_BEFORE_1982 ||
                 coin.getWeight() == PENNY_WEIGHT_GRAMS_MINTED_AFTER_1982);
        return isPenny;
    }
    
    public boolean isNickle(Coin coin){
        boolean isNickle = coin.getDiameter() == NICKLE_DIAMETER_MILLIMETER && 
                coin.getThickness() == NICKLE_THICKNESS_MILLIMETER && 
                coin.getWeight() == NICKLE_WEIGHT_GRAMS;
        return isNickle;
    }
    
    public boolean isDime(Coin coin){
        boolean isDime = coin.getDiameter() == DIME_DIAMETER_MILLIMETER && 
                coin.getThickness() == DIME_THICKNESS_MILLIMETER && 
                coin.getWeight() == DIME_WEIGHT_GRAMS;
        return isDime;
    }
    
    public boolean isQuarter(Coin coin){
        boolean isQuarter = coin.getDiameter() == QUARTER_DIAMETER_MILLIMETER && 
                coin.getThickness() == QUARTER_THICKNESS_MILLIMETER && 
                coin.getWeight() == QUARTER_WEIGHT_GRAMS;
        return isQuarter;
    }
}
