/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.check1two.vendingmachine.coinacceptor;

import java.math.BigDecimal;

/**
 *
 * @author manueltijerino
 */
public class CoinAcceptor {

    private final CoinValidator coinValidator = new CoinValidator();
    
    private BigDecimal currentAmount = new BigDecimal(0);
    
    private final String DEFAULT_MESSAGE = "INSERT COIN";
    
    public BigDecimal getCurrentAmount() {
        return currentAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
    
    public void insert(Coin coin){
        if(coinValidator.isValid(coin)){
            double value = coinValidator.getValue(coin);
            BigDecimal bd = new BigDecimal(value);
            currentAmount = currentAmount.add(bd);
        }
    }
    
    public void reset(){
        currentAmount = new BigDecimal(0);
    }
    
    public String getDisplay(){
        if(getCurrentAmount().doubleValue() == 0){
            return DEFAULT_MESSAGE;
        }
        return String.valueOf(getCurrentAmount());
    }
}
